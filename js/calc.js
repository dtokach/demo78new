days    = 0,
di      = 0,
rate    = 82.212,
percent = 0;
var
    hashpower         = 0,

    summin            = 0,
    summax            = 0,
    sumsecond         = 0,
    sumthird          = 0,

    currentsum        = 0,

    percentfirst      = 0,
    percentsecond     = 0,
    percentthird      = 0,
    percentlast       = 0;

$(document).ready(function () {
    p = $('.calc');

    summin      = parseFloat(get_data_args(p).summin);
    sumsecond   = parseFloat(get_data_args(p).sumsecond);
    sumthird    = parseFloat(get_data_args(p).sumthird);
    summax      = parseFloat(get_data_args(p).summax);

    percentfirst    = parseFloat(get_data_args(p).percentfirst);
    percentsecond   = parseFloat(get_data_args(p).percentsecond);
    percentthird    = parseFloat(get_data_args(p).percentthird);
    percentlast     = parseFloat(get_data_args(p).percentlast);

    if (document.getElementsByClassName('range01').length > 0) {
        $(".range01").slider("destroy");
    }

    $('.sl_part #inv_money1').html(summin + '&nbsp;usd');
    $('.sl_part #inv_money2').html(sumsecond + '&nbsp;usd');
    $('.sl_part #inv_money3').html(sumthird + '&nbsp;usd');
    $('.sl_part #inv_money4').html(summax + '&nbsp;usd');

    $('.sl_part #percent1').html(percentfirst + '&nbsp;%');
    $('.sl_part #percent2').html(percentsecond + '&nbsp;%');
    $('.sl_part #percent3').html(percentthird + '&nbsp;%');
    $('.sl_part #percent4').html(percentlast + '&nbsp;%');

    console.log(percent, 'per');
    if (document.getElementsByClassName('range01').length > 0) { // те такие блоки есть
        $(".range01").slider({
            range: true,
            min: +summin,
            max: +summax,
            values: [+summin, currentsum],
            disabled: true,
            classes: {
                "ui-slider-handle": "set"
            },
            slide: function (event, ui) {
                currentsum = +ui.value;
                calc();
            }
        });
    }
    calc();
});

function calc() {
    var profit     = currentsum * percent * days / 100;
        hashpower  = profit * rate;

    $('.profit').html(+profit.toFixed(2));
    $('.hashpower').html(+hashpower.toFixed(2) + ' GH/s');
}


function get_data_args(_this) {
  var arr = {}, regV = /data\-/gi;
  $(_this).each(function () {
    $.map(this.attributes, function (attribute) {
      if (attribute.name.match(regV)) {
        arr[attribute.name.replace('data-', '')] = attribute.value;
      }
    });
  });
  return arr;
}